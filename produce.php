<?php
/**
 * Plugin Name: Produce
 * Version: 0.2
 * Description: Fill your Crate theme with produce!
 * Author: Ben Byrne, Cornershop Creative
 * Author URI: https://cornershopcreative.com
 * Plugin URI: https://cornershopcreative.com
 * Text Domain: produce
 * Domain Path: /lang
 *
 * @package Produce
 */

if ( defined( 'WP_CLI' ) && WP_CLI ) :

	/**
	 * Crate_Produce class is the parent class for all Produce commands.
	 *
	 * @extends WP_CLI_Command
	 */
	class Crate_Produce extends WP_CLI_Command {

		/**
		 * Populate some variables on construction.
		 *
		 * @access public
		 * @return void
		 */
		function __construct() {
			$this->crate_dir          = get_theme_root() . '/crate';
			$this->crate_template_dir = $this->crate_dir . '/template-parts';
			$this->crate_js_dir       = $this->crate_dir . '/_src/js';
			$this->crate_sass_dir     = $this->crate_dir . '/_src/scss';
			$this->crate_inc_dir      = $this->crate_dir . '/inc';
			$this->dir                = __DIR__; // Won't work with php < 5.3.0.
			$this->files_queued       = array();
			$this->files_added        = array();
			parent::__construct();
		}


		/**
		 * Utility for gathering input from STDIN
		 *
		 * Provide a string that's the question you want to pose to the user, and then optionally an array
		 * of acceptable answers, followed by an optional default if you wish users to not always need to specify.
		 *
		 * Example: prompt('Enter your first name: ');
		 * Example: prompt('What will you never do?', array('Give you up','Let you down', 'Desert you'), 'Desert you');
		 *
		 * @access protected
		 * @param string $text The question to display to the user.
		 * @param array  $options Array of valid responses user will be presented with (default: NULL).
		 * @param string $default The answer that will be chosen if the user provides no input (default: NULL).
		 * @return string The (valid) answer received from the user.
		 * @todo Check that if an array of option is present that the default value is one of them.
		 */
		protected function prompt( $text, $options = null, $default = null ) {

			$echo = $text;
			$answer = false;

			if ( is_array( $options ) ) {
				$echo .= ' (' . implode( ',', $options ) . ')';
			}

			if ( $default ) {
				$echo .= ' [' . $default . ']';
			}

			$echo = trim( $echo ) . ' ';

			while ( ! $answer ) {
				fwrite( STDOUT, $echo );
				$answer = trim( fgets( STDIN ) );

				if ( $default && empty( $answer ) ) { $answer = $default; }

				if ( is_array( $options ) && ! in_array( $answer, $options, true ) && $answer !== $default ) {
					WP_CLI::error( 'Invalid response', false );
					$answer = false;
				}
			}

			return $answer;

		}


		/**
		 * Add a js/php/sass/mustache file to the list of files to be injected
		 * $file can be an array of [ source-file, destination-file, (data if mustache) ]
		 * $file can also be a string in which case source & destination paths are inferred by suffix
		 *
		 * @access protected
		 * @param string $source The file from Produce to queue up for injection.
		 *   Can be a path relative to the Produce root, or just a file name if it's a known extension (js/sass/scss/php/inc/json/mustache).
		 * @param string $destination The path/and/file destination of where the source file will be put in Crate.
		 *   If left blank, destination will try to be computed based on file type $source.
		 * @param array  $data (default: false) An array of data necessary for populating a mustache template.
		 */
		protected function enqueue( $source, $destination = false, $data = false ) {

			// Determine the filetype.
			$filetype = pathinfo( $source, PATHINFO_EXTENSION );
			$sourcefilename = basename( $source );

			// If the source file doesn't exist and we don't know where to go looking for it.
			if ( ! file_exists( $this->dir . $source ) ) {

				// Let's go look for our file....
				$suffixes_to_directories = array(
				'js'       => 'js',
				'sass'     => 'sass',
				'scss'     => 'sass',
				'php'      => 'templates',
				'inc'      => 'includes',
				'json'     => 'fieldgroups',
				'mustache' => 'mustache',
				);

				// We can't go looking for a file if we don't know its extension or its extension has no home.
				if ( ! $filetype || ! array_key_exists( $filetype, $suffixes_to_directories ) ) {
					WP_CLI::error( 'Invalid source file specified: ' . $sourcefilename );
				}

				// Try again to find the source file.
				$new_source = $this->dir . '/' . $suffixes_to_directories[ $filetype ] . '/' . $source;
				if ( ! file_exists( $new_source ) ) {
					WP_CLI::error( 'Source file could not be found: ' . $sourcefilename );
				} else {
					$source = $new_source;
				}
			}

			// We have a source file. Yay! Let's read in the file's contents.
			$contents = false;
			if ( 'mustache' === $filetype ) {
				$contents = $this->get_mustache( $source, $data );
			} else {
				$contents = $this->get_file( $source );
			}

			// If it contained nothing, that's probably bad.
			if ( ! $contents ) {
				WP_CLI::warning( 'Source file' . $sourcefilename . ' appears to have been empty.' );
			}

			// Now let's figure out a destination for the file. Note that ACF json files are a special case.
			if ( ! $destination ) {

				// We'll need a known filetype if we're computing a destination.
				if ( ! $filetype || ! array_key_exists( $filetype, $suffixes_to_directories ) ) {
					WP_CLI::error( 'Unable to determine destination for file: ' . $sourcefilename );
				}

				switch ( $filetype ) {
					case 'json':
						$destination = $source;
						break;
					case 'sass':
					case 'scss':
						$destination = $this->crate_sass_dir . '/' . $sourcefilename;
						break;
					case 'php':
						$destination = $this->crate_template_dir . '/' . $sourcefilename;
						break;
					case 'inc':
						$destination = $this->crate_inc_dir . '/' . $sourcefilename;
						break;
					case 'js':
						$destination = $this->crate_js_dir . '/' . $sourcefilename;
						break;
				}
			} else {
				$destination = $this->crate_dir . $destination;
			}

			// We now have a $content and a (hopefully) valid destination. Let's add it to our array.
			$this->files_queued[ $destination ] = $contents;

		}


		/**
		 * Inject enqueued files into Crate and (optionally) notify users of what was injected.
		 *
		 * This is typically called immediately after all files have been enqueue()'d
		 *
		 * @access protected
		 * @param bool $force (default: false) Whether to overwrite any existing files, if present.
		 * @param bool $display_results (default: true) Whether to display a log of written files to the user.
		 * @return void
		 */
		protected function produce( $force = false, $display_results = true ) {

			if ( ! count( $this->files_queued ) ) {
				WP_CLI::error( 'No files enqueued for injection into Crate' );
			}

			$this->files_added = $this->add_files( $this->files_queued, $force );

			if ( $display_results ) {
				$this->list_added_files();
			}

		}


		/**
		 * Output to STDOUT what files were just injected into Crate.
		 *
		 * @access private
		 * @return void
		 */
		private function list_added_files() {

			if ( ! count( $this->files_added ) ) {

				WP_CLI::warning( 'No files were added to Crate' );

			} else {

				$message = 'The following files were added to Crate:' . PHP_EOL . PHP_EOL;
				foreach ( $this->files_added as $filename ) {
					$message .= '   ' . $filename . PHP_EOL;
				}
				WP_CLI::success( $message );

			}

		}


		/**
		 * Tell user what needs to be manually done next.
		 *
		 * This is typically called immediately after produce() to inform the user that some code
		 * need to be manually added/changed within the Crate codebase to completely implement
		 * whatever was just injected.
		 *
		 * @access protected
		 * @param array|string $code Line or lines of code that need to be inserted.
		 * @param string       $destination Name of the file in Crate where the provided code needs to be added.
		 * @return void
		 */
		protected function next_steps( $code, $destination ) {

			if ( ! is_array( $code ) ) {
				$code = array( $code );
			}

			$message = 'Please manually add the following to ' . $destination . ':' . PHP_EOL . PHP_EOL;
			foreach ( $code as $line ) {
				$message .= '   ' . $line . PHP_EOL;
			}
			WP_CLI::log( $message );

		}


		/**
		 * Get a mustache file, processed the contents, and return the result.
		 *
		 * Note that mustache_render() is defined in wp_cli/php/utils.php, look there for more.
		 *
		 * @access private
		 * @param string $filename Name of the mustache file to load.
		 * @param array  $data Data to be used in processing the file.
		 * @return string The processed mustache template.
		 */
		private function get_mustache( $filename, $data ) {

			if ( ! file_exists( $filename ) ) {
				WP_CLI::error( "Unable to read file: $file" );
			}

			return \WP_CLI\Utils\mustache_render( $filename, $data );
		}


		/**
		 * Get a file's contents and return them as a string.
		 *
		 * @access private
		 * @param string $filename Name of the mustache file to read into a string.
		 * @return string The contents of the file.
		 */
		private function get_file( $filename ) {

			$contents = file_get_contents( $filename );

			if ( ! file_get_contents( $filename ) ) {
				WP_CLI::error( "Unable to read file: $filename" );
			}

			return $contents;
		}


		/**
		 * Actually write files into Crate's filesystem.
		 *
		 * Note that files destined for the 'fieldgroups' directory are assumed to be ACF-JSON
		 * fieldsets and are NOT written to the filesystem, but actually processed by ACF
		 * into the database. In those cases, the entry in the return array is actually basic
		 * information about the field group, and not a file name.
		 *
		 * @access private
		 * @param array $files_and_contents Array consisting of ['destination filename' => 'contents'] pairs.
		 * @param bool  $force Whether or not to overwrite existing files, if any.
		 * @return array An array of strings of the names of the files that were written.
		 */
		private function add_files( $files_and_contents, $force ) {
			$added_files = array();

			foreach ( $files_and_contents as $filename => $contents ) {

				// Note: acf-json is a special case: run a command, don't copy a file.
				if ( strpos( $filename, 'fieldgroups' ) !== false ) {

					$fieldgroups = $this->import_acf_json( $contents, $filename );
					foreach ( $fieldgroups as $group_title ) {
						$added_files[] = "ACF field group '$group_title'";
					}
				} else {

					$should_write_file = $force || $this->prompt_overwrite( $filename );

					if ( ! $should_write_file ) {
						continue;
					}

					if ( ! file_put_contents( $filename, $contents ) ) {
						WP_CLI::error( "Error creating file: $filename" );
					} else {
						$added_files[] = $filename;
					}
				}
			}

			return $added_files;
		}


		/**
		 * Ask user via STDIN if it's okay to overwrite an existing file.
		 *
		 * @access private
		 * @param string $filename The path/and/filename that we want to overwrite.
		 * @return bool Whether or not to perform the overwrite.
		 */
		private function prompt_overwrite( $filename ) {

			$should_write_file = false;

			if ( ! file_exists( $filename ) ) { return true; }

			WP_CLI::warning( 'File already exists: ' . $filename );

			$answer = self::prompt( 'Skip this file, or overwrite it?', array( 's', 'o' ) );

			if ( 'o' === $answer ) { $should_write_file = true; }

			$action = $should_write_file ? 'Overwriting' : 'Skipping';
			WP_CLI::log( $action );

			return $should_write_file;
		}


		/**
		 * Perform an import of some JSON sstring into to the WP database as Advanced Custom Fields.
		 *
		 * @access private
		 * @param mixed $contents The JSON to be imported into the database.
		 * @param mixed $filename The filename that the JSON came from. Used to tell users where problems are coming from;
		 *  is not read from or written to.
		 * @return array A list of the titles of all field groups that were successfully added.
		 * @todo Right now this will re-import a field group that already exists. We should key into --force and do this smarter.
		 */
		private function import_acf_json( $contents, $filename ) {

			// This logic is all lifted from the ACF-CLI package, specifically:
			// https://github.com/hoppinger/advanced-custom-fields-wpcli/blob/master/src/Fieldgroup.php
			// https://github.com/hoppinger/advanced-custom-fields-wpcli/blob/master/src/Field.php
			// The file advanced-custom-fields-pro/admin/settings-tools.php also is relevant.
			$json = json_decode( $contents, true );

			if ( empty( $json ) ) {
				WP_CLI::error( "$filename appears to be invalid JSON.", false );
				WP_CLI::error( $contents );
			}

			// A single group should be within an array.
			if ( isset( $json['key'] ) ) {
				$json = array( $json );
			}

			// Vars.
			$ref          = array();
			$order        = array();
			$groups_added = array();

			foreach ( $json as $new_field_group ) {
				// Separate field data from field group data. We'll add the field group
				// first, then import the fields one at a time.
				$fields = acf_extract_var( $new_field_group, 'fields' );

				// Check for existing field groups that may be duplicates of the one
				// we're trying to add.
				$existing_field_groups = array();

				// Check all existing field groups for matching keys or names. This
				// method will detect multiple matches (just to be safe), whereas using
				// acf_get_field_group() would only find one match at most.
				$all_field_groups = acf_get_field_groups();
				foreach ( $all_field_groups as $potential_match ) {
					if ( ( $potential_match['key'] === $new_field_group['key'] )
						|| ( $potential_match['title'] === $new_field_group['title'] ) ) {
						$existing_field_groups[] = $potential_match;
					}
				}

				// If there's already one or more field group with the same name or
				// key, ask the user what they want to do.
				$n_existing = count( $existing_field_groups );
				if ( $n_existing > 0 ) {

					$dupe_or_dupes = ( $n_existing === 1 ? 'duplicate' : 'duplicates' );
					WP_CLI::warning( "Found $n_existing possible $dupe_or_dupes for field group '{$new_field_group['title']}' ({$new_field_group['key']})." );

					$add_anyway = self::prompt( 'Do you still want to add this group?', array( 'y', 'n' ), 'y' );

					// If the user decides not to add this field group after all, bail.
					if ( 'n' === $add_anyway ) {
						WP_CLI::log( "Skipping field group '{$new_field_group['title']}' ({$new_field_group['key']})." );
						return false;
					}

					// Ask the user what they want to do with each field group.
					foreach ( $existing_field_groups as $existing_field_group ) {

						$action = self::prompt(
							"Existing field group '{$existing_field_group['title']}' ({$existing_field_group['key']}): Delete this group, or keep it?",
							array( 'd', 'k' ),
							'd'
						);

						if ( 'd' === $action ) {
							// If the user chose to delete this group, delete it.
							acf_delete_field_group( $existing_field_group['key'] );
						} else {
							// If the user chose to keep this group, but its key matches the
							// group we're trying to add, then force ACF to change the new
							// group's key when it gets added.
							if ( $existing_field_group['key'] === $new_field_group['key'] ) {
								WP_CLI::log( 'Updating key for new field group in order to prevent collisions.' );
								// If 'key' is unset, ACF will automatically generate a new
								// group key when it adds the new field group.
								unset( $new_field_group['key'] );
							}
						}
					}
				}

				// Save field group.
				$new_field_group = acf_update_field_group( $new_field_group );

				// Add to ref.
				$ref[ $new_field_group['key'] ] = $new_field_group['ID'];
				// Add to order.
				$order[ $new_field_group['ID'] ] = 0;

				// Sanitize field data.
				$fields = acf_prepare_fields_for_import( $fields );

				// Add individual fields.
				foreach ( $fields as $field ) {
					// Add parent.
					if ( empty( $field['parent'] ) ) {
						$field['parent'] = $new_field_group['ID'];
					} elseif ( isset( $ref[ $field['parent'] ] ) ) {
						$field['parent'] = $ref[ $field['parent'] ];
					}

					// Add field menu_order.
					if ( ! isset( $order[ $field['parent'] ] ) ) {
						$order[ $field['parent'] ] = 0;
					}

					$field['menu_order'] = $order[ $field['parent'] ];
					$order[ $field['parent'] ]++;

					// Save field.
					$field = acf_update_field( $field );

					// Add to ref.
					$ref[ $field['key'] ] = $field['ID'];
				}

				// Add group to running total of groups added.
				$groups_added[] = $new_field_group['title'];
			}
			return $groups_added;
		}

		/**
		 * Convert a 'normal-looking' keyless PHP array into one that mustache can handle as
		 * as 'non-empty list' per https://github.com/bobthecow/mustache.php/wiki/Mustache-Tags
		 *
		 * @access protected
		 * @param array $items The array list of items to reformat.
		 * @param string $context Name for the outer array key. Probably matches the parameter's variable name, but there's no good way to get that.
		 * @param string $key The key that each item should have in its sub-array
		 * @return array A new array organized for Mustache lists.
		 */
		protected function mustache_array( $items, $context, $key ) {

			$new_array = array(
				$context => array(),
			);

			foreach( $items as $value ) {
				$new_array[ $context ][] = array( $key => trim( $value ) );
			}

			return $new_array;

		}
	}


	/**
	 * Load up all our commands.
	 */
	foreach ( glob( __DIR__ . '/commands/*.php' ) as $filename ) {
		include $filename;
	}

	// End check for WP_CLI.
endif;
