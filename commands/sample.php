<?php
/**
 * Implements a sample command to demonstrate usage of Crate_Produce methods.
 *
 * @package Produce
 */

/**
 * This sample command does absolutely nothing of value.
 *
 * @extends Crate_Produce
 */
class Sample_Command extends Crate_Produce {

	/**
	 * Outputs sample code into Crate for demonstration purposes only.
	 *
	 * ## OPTIONS
	 *
	 * <color>
	 * : The color value to include in the template file.
	 *
	 * <whichfiles>
	 * : Specify "all" to include acf import, some other value to omit ACF.
	 *
	 * [--skip=<filetype,filetype>]
	 * : Whether or not to skip certain filetype(s). This sample only honors 'include'
	 *
	 * [--force]
	 * : Force overwrite of existing files, if any.
	 *
	 * ## EXAMPLES
	 *
	 *     wp produce sample green all --force
	 *
	 *     wp produce sample #CCC some --other=cheeto --no-output
	 */
	public function __invoke( $args, $assoc_args ) {

		// Example use of prompt.
		$response = self::prompt(
			'Are you ready to do this?',
			array( 'Yes', 'No', 'Maybe' ),
			'Yes'
		);

		WP_CLI::success( 'When I asked if you were ready, you said ' . $response );

		// Example enqueues.
		// You can enqueue a single file and produce will figure out where to put it.
		$this->enqueue( 'sample.js' );

		// Or you can enqueue and specify destination within crate.
		$this->enqueue( 'sample.sass', '/sass/_sample.sass' );

		// Enqueue something conditionally based on the first argument.
		list( $first_arg, $second_arg ) = $args;

		if ( 'all' === $second_arg ) {
			$this->enqueue( 'sample.json' );
		}

		// Suppress something if specified in comma-separated list.
		$skip_types = array();
		if ( array_key_exists( 'skip', $assoc_args ) ) {
			$skip_types = explode( ',', $assoc_args['skip'] );
		}

		if ( ! in_array( 'include', $skip_types, true ) ) {
			$this->enqueue( 'sample.inc' );
		}

		// Got a mustache file? Provide a data array as third arg.
		$data = array( 'color' => $first_arg );
		$this->enqueue( 'sample.mustache', '/templates/sample.php', $data );

		// Once everything is enqueued, produce it. You'll want to let it know if it should overwrite existing files.
		// By default produce() will list what files were written. Second arg to produce() can suppress that output.
		$do_overwrite   = isset( $assoc_args['force'] ) ? true : false;
		$output_results = isset( $assoc_args['quiet'] ) ? false : true;
		$this->produce( $do_overwrite, $output_results );

		// Oftentimes the user will need to manually add some code elsewhere into crate, such as updating crate.js.
		// Call next_steps() to let them know what to add and where.
		$this->next_steps(
			"@include 'sample'",
			'core.sass'
		);

		// Got multiple lines? pass each as an array in the first argument...
		$this->next_steps(
			array( "var sample = require('sample-renamed.js');", 'sample();' ),
			'crate.js'
		);

		// Probably best to be courteous at the end.
		WP_CLI::success( 'I did that thing you wanted.' );

	}
}

WP_CLI::add_command( 'produce sample', 'Sample_Command' );
