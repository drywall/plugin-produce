<?php
/**
 * Implements a robust ACF field collection known as 'sections' for building advanced layouts.
 *
 * @package Produce
 */

/**
 * This command adds a bevy of ACF fields for 'Sections', and adds template files
 * to Crate for outputting them, as well as some enhancements for search.
 *
 * @extends Crate_Produce
 */
class Sections_Command extends Crate_Produce {

	/**
	 * Implements fields, templates and SCSS for several post "Section" layouts.
	 *
	 * ## OPTIONS
	 *
	 * [--post_types=<type,type>]
	 * : Comma-separated list of post types Sections should be available on.
	 *
	 * [--palette=<hexcolor,hexcolor>]
	 * : Comma-separated list of up to six hex colors (e.g. #f90) to include in color picker.
	 *
	 * [--force]
	 * : Force overwrite of existing files, if any.
	 *
	 * ## EXAMPLES
	 *
	 *     wp produce sections
	 *
	 *     wp produce sections --post-types=post,page --palette=#f00,#0f0,#00f,#f0f
	 */
	public function __invoke( $args, $assoc_args ) {

		// Defaults
		$post_types = array( 'page' );
		$palette = $base_palette = array( '#c44', '#c94', '#cc4', '#9c4', '#49c', '#94c' );

		// Set the post types sections should be applied to.
		// When we get to the mustache template, $post_types should be a php array
		if ( array_key_exists( 'post_types', $assoc_args ) ) {

			// If post types were specified, override the default automatically
			$post_types = explode( ',', $assoc_args['post_types'] );

		} else {

			// If post types weren't specified, get a list of current post types and prompt user
			$available_types = get_post_types( array( 'public' => true ), 'objects' );
			$type_choices = array();
			foreach ( $available_types as $post_type ) {
				$type_choices[] = $post_type->name;
			}
			$response = self::prompt(
				'Enter a comma-separated list of post types that should have sections (current types are ' . implode( ', ', $type_choices ) . '):',
				null,
				'page'
			);
			// @todo: validate this a little bit, maybe?
			$post_types = explode( ',', $response );

		}//end if

		// Set the color palette that should be used for section_bg_color picker
		// When we get to the mustache template, palette should be a php array
		if ( array_key_exists( 'palette', $assoc_args ) ) {

			// If palette colors were specified, override the default automatically
			$palette = explode( ',', $assoc_args['palette'] );

		} else {

			// If palette colors weren't specified, prompt user but don't be picky
			$user_colors = self::prompt(
				'Enter a comma-separated list of up to six hex colors to include in bg color picker:',
				null,
				implode( ',', $palette )
			);
			$palette = explode( ',', $user_colors );

		}
		// Make sure palette isn't too long: should be no more than six
		$palette = array_slice( $palette, 0, 6 );

		// Sanitize the color palette, removing weird/empty options
		foreach ( $palette as $key => &$color ) {
			$color = sanitize_hex_color( trim( $color ) );
			if ( empty( $color ) ) {
				unset( $palette[ $key ] );
			}
		}

		// If the color palette is too short, add stuff to complete it.
		while ( count( $palette ) < 6 ) {
			$palette[] = array_shift( $base_palette );
		}

		/**
		 * Line up the PHP files!
		 */

		// Process the post types in the ACF field definitions
		$this->enqueue( 'sections.json.mustache', '/fieldgroups/sections.json', $this->mustache_array( $post_types, 'post_types', 'type' ) );

		// Process the color palette mustache for js
		$this->enqueue( 'sections-admin.js.mustache', '/js/sections-admin.js', $this->mustache_array( $palette, 'palette', 'color' ) );

		$this->enqueue( 'sections.inc' );
		$this->enqueue( '_sections.scss' );

		// Process all the templates
		$this->enqueue( 'sections.php' );

		// @todo: write template for content collection
		$section_templates = array( 'columns', 'hero', 'image_features', 'slider', 'wysiwyg', 'content_collection' );
		foreach ( $section_templates as $template ) {
			$this->enqueue( "section-$template.php" );
		}

		$do_overwrite   = isset( $assoc_args['force'] ) ? true : false;
		$output_results = isset( $assoc_args['quiet'] ) ? false : true;
		$this->produce( $do_overwrite, $output_results );

		// Tell the user to add _sections.scss to crate.scss
		$this->next_steps(
			"@import 'sections';",
			'_src/scss/crate.scss'
		);

		// Oftentimes the user will need to manually add some code elsewhere into crate, such as updating crate.js.
		$this->next_steps(
			"<?php get_template_part( 'template-parts/sections' ); ?>",
			'template-parts/content{-type}.php'
		);

		WP_CLI::log( "You'll also need to install the cycle2 jquery plugin (if you haven't already) for the slider to work!" );
		WP_CLI::log( "Run 'bower install jquery-cycle2' from the crate directory, then..." );

		$this->next_steps(
			"var cycle = require('jquery-cycle2');",
			'_src/js/index.js'
		);

		// Probably best to be courteous at the end.
		WP_CLI::success( 'Sections implemented.' );

	}
}

WP_CLI::add_command( 'produce sections', 'Sections_Command' );
