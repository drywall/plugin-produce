<?php
/**
 * Implements a command to create an ACF-based Site Options page with a handful
 * of commonly requested features.
 *
 * @package Produce
 */

/**
 * This command adds ACF fields for a Site Options page, and adds code to Crate
 * to create the page and to implement some of the options (other options will
 * need to be implemented by devs on a per-theme basis).
 *
 * @extends Crate_Produce
 */
class Site_Options_Command extends Crate_Produce {

	/**
	 * Add ACF fields and an .inc file for a Site Options page.
	 *
	 * ## OPTIONS
	 *
	 * [--force]
	 * : Force overwrite of existing files, if any.
	 *
	 * ## EXAMPLES
	 *
	 *     wp produce site-options
	 */
	public function __invoke( $args, $assoc_args ) {

		$this->enqueue( 'site-options.json' );
		$this->enqueue( 'site-options.inc' );

		// Once everything is enqueued, produce it.
		$do_overwrite   = isset( $assoc_args['force'] ) ? true : false;
		$output_results = isset( $assoc_args['quiet'] ) ? false : true;
		$this->produce( $do_overwrite, $output_results );

		WP_CLI::log(
"If you'd like to allow users to set different default featured images for
different post types, then for each post type that should have its own default
image, create a copy of the 'default_post_thumbnail_id' field in the Site
Options field group, and change 'post' in the field name to the name of the
post type, e.g. 'default_press-release_thumbnail_id'.
" );

		WP_CLI::log(
"To take advantage of the Error Page feature, set a custom error page in the
Site Settings. You can now use the Loop (have_posts(), the_post()...) in the
404.php template in order to display the designated error page\'s content,
custom fields, etc.
" );

		$this->next_steps(
			array(
				'crate_copyright_text();',
				'crate_contact_info();',
			),
			'footer.php'
		);

		$this->next_steps(
			array(
				'crate_social_links( [li-class-prefix], [link-target], [ul-classes], [ul-id] );',
			),
			'any template where you want an HTML <ul> of social links'
		);

		$this->next_steps(
			array(
				'foreach ( crate_get_social_links() as $link ) :',
				'  // Do something with $link',
				'endforeach;',
			),
			'any template where you want custom social link markup'
		);

		$this->next_steps(
			'crate_donate_url(); // Or crate_get_donate_url() to return a value.',
			'any template where you want the Donate URL'
		);

		$this->next_steps(
			'crate_google_maps_api_key(); // Or crate_get_google_maps_api_key() to return a value.',
			'any template where you need the Google Maps API key'
		);

		$this->next_steps(
			'crate_fb_app_id(); // Or crate_get_fb_app_id() to return a value.',
			'any template where you need the Facebook App ID'
		);
	}
}

WP_CLI::add_command( 'produce site-options', 'Site_Options_Command' );
