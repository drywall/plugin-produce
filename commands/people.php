<?php
/**
 * Implements a post type called 'People'
 *
 * @package Produce
 */

/**
 * This command registers a post type called People, a renamable taxonomy called Groups by default,
 * custom fields in the post type for personal details, and single and archive templates.
 *
 * @extends Crate_Produce
 */
class People_Command extends Crate_Produce {

	/**
	 * Creates a 'People' post type along with related fields, taxonomy, and templates.
	 *
	 * ## OPTIONS
	 *
	 * [--slug=<string>]
	 * : A short string for the rewrite slug of the Article post type. Defaults to 'person'
	 *
	 * [--groups=<term,term>]
	 * : Comma-separated list of groups to prepopulate in the group taxonomy
	 *
	 * [--force]
	 * : Force overwrite of existing files, if any.
	 *
	 * ## EXAMPLES
	 *
	 *     wp produce people --slug=staff --groups=Communications,Fundraising,Board
	 *
	 *     wp produce people --slug=staff --groups=Communications,Fundraising,Board
	 */
	public function __invoke( $args, $assoc_args ) {

		// Defaults
		$slug  = 'people';
		$terms = array( 'Staff', 'Board', 'Other' );
		$taxonomy = array( 'Groups', 'group' );

		// Override the default slug, if specified
		if ( array_key_exists( 'slug', $assoc_args ) ) {

			$slug = $assoc_args['slug'];

		}

		// Set the article type terms, if specified
		if ( array_key_exists( 'groups', $assoc_args ) ) {
			$terms = explode( ',', $assoc_args['groups'] );
		}

		// Set the taxonomy name and label
		$taxonomy[0] = self::prompt(
			'What should the human-readable label of the taxonomy be?',
			false,
			$taxonomy[0]
		);

		$taxonomy[1] = self::prompt(
			'What should the machine name of the taxonomy be?',
			false,
			$taxonomy[1]
		);

		// Stringify the $terms array for inclusion in mustache template
		$terms_string = '"' . implode( '", "', $terms ) . '"';

		/**
		 * Line up the files!
		 */

		// Process the main file that registers the post type
		// @todo write template tag helpers
		$this->enqueue( 'people.mustache', '/inc/post-type-people.inc', array(
			'slug'         => $slug,
			'terms'        => $terms_string,
			'tax_label'    => $taxonomy[0],
			'tax_name'     => $taxonomy[1],
		) );

		// Process the ACF field definitions
		$this->enqueue( 'people-detail-fields.json' );
		$this->enqueue( 'people-contact-fields.json' );

		// Process the SCSS
		$this->enqueue( '_people.scss', '/_src/scss/templates/_people.scss' );

		// Process the templates.
		$this->enqueue( 'single-person.php', '/single-person.php' );
		$this->enqueue( 'archive-person.php' );
		$this->enqueue( 'content-person.php', '/template-parts/content-person.php' );

		$do_overwrite   = isset( $assoc_args['force'] ) ? true : false;
		$output_results = isset( $assoc_args['quiet'] ) ? false : true;
		$this->produce( $do_overwrite, $output_results );

		// Flush rewrite rules so our new slug works!
		// We can't use flush_rewrite_rules() because our new .inc hasn't been loaded here yet.
		WP_CLI::runcommand( 'rewrite flush' );

		// Tell the user to wire up our new SCSS
		$this->next_steps(
			'@import "people";',
			'_src/scss/templates/_templates.scss'
		);

		// Probably best to be courteous at the end.
		WP_CLI::success( 'Person post type implemented.' );

	}
}

WP_CLI::add_command( 'produce people', 'People_Command' );
