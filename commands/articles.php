<?php
/**
 * Implements a post type called 'Articles'
 *
 * @package Produce
 */

/**
 * This command registers a post type called Articles, a taxonomy called Article Type,
 * some custom fields for article posts, some templates, and so on.
 *
 * @extends Crate_Produce
 */
class Articles_Command extends Crate_Produce {

	/**
	 * Creates an 'Articles' post type along with related fields, taxonomy, and templates.
	 *
	 * ## OPTIONS
	 *
	 * [--slug=<string>]
	 * : A short string for the rewrite slug of the Article post type. Defaults to 'article'
	 *
	 * [--types=<term,term>]
	 * : Comma-separated list of article types to prepopulate in the article type taxonomy
	 *
	 * [--urls=<rewrite|redirect|both|nothing>]
	 * : Define handling of external urls: Rewrite permalinks, redirect from permalink, do both, or do neither
	 *
	 * [--force]
	 * : Force overwrite of existing files, if any.
	 *
	 * ## EXAMPLES
	 *
	 *     wp produce articles
	 *
	 *     wp produce articles --slug=content --types=News,Press Release,Resource
	 */
	public function __invoke( $args, $assoc_args ) {

		// Defaults
		$slug  = 'article';
		$terms = array( 'News', 'Press Release', 'Publication', 'Newsletter' );
		$url_handler_schemes = array( 'rewrite', 'redirect', 'both', 'nothing' );

		// Override the default slug, if specified
		if ( array_key_exists( 'slug', $assoc_args ) ) {

			$slug = $assoc_args['slug'];

		}

		// Set the article type terms, if specified
		if ( array_key_exists( 'types', $assoc_args ) ) {

			$terms = explode( ',', $assoc_args['types'] );

		}

		// Set the external URL handers, or request if unspecified
		if ( array_key_exists( 'urls', $assoc_args ) && in_array( $assoc_args['urls'], $url_handler_schemes, true ) ) {

			$urls = $assoc_args['urls'];

		} else {

			$urls = self::prompt(
				'What should happen when an article has an external URL?',
				array( 'rewrite', 'redirect', 'both', 'nothing' ),
				'both'
			);

		}

		// Stringify the $terms array for inclusion in mustache template
		$terms_string = '"' . implode( '", "', $terms ) . '"';

		/**
		 * Line up the files!
		 */

		// Process the main file that registers the post type
		// @todo write template tag helpers
		$this->enqueue( 'articles.mustache', '/inc/post-type-articles.inc', array(
			'slug'        => $slug,
			'terms'       => $terms_string,
			'url_handling' => $urls,
		) );

		// Process the ACF field definitions
		$this->enqueue( 'article-fields.json' );

		// Process the single template.
		$this->enqueue( 'single-article.php' );

		// @todo write and **optionally** include archive-article.php
		$do_overwrite   = isset( $assoc_args['force'] ) ? true : false;
		$output_results = isset( $assoc_args['quiet'] ) ? false : true;
		$this->produce( $do_overwrite, $output_results );

		// Flush rewrite rules so our new slug works!
		// We can't use flush_rewrite_rules() because our new .inc hasn't been loaded here yet.
		WP_CLI::runcommand( 'rewrite flush' );

		// Probably best to be courteous at the end.
		WP_CLI::success( 'Articles implemented.' );

	}
}

WP_CLI::add_command( 'produce articles', 'Articles_Command' );
