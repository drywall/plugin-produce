<?php
/**
 * Implements a sample command to let Crate allow SVGs in the media library
 *
 * @package Produce
 */

/**
 * This sample command does absolutely nothing of value.
 *
 * @extends Crate_Produce
 */
class SVG_Upload_Command extends Crate_Produce {

	/**
	 * Adds svg-upload.inc to Crate's /inc/ directory to allow SVG uploads into the Media Library.
	 *
	 * ## OPTIONS
	 *
	 * [--force]
	 * : Force overwrite of existing files, if any.
	 */
	public function __invoke( $args, $assoc_args ) {

		$this->enqueue( 'svg-upload.inc' );

		$do_overwrite   = isset( $assoc_args['force'] ) ? true : false;
		$output_results = isset( $assoc_args['quiet'] ) ? false : true;

		$this->produce( $do_overwrite, $output_results );

		// Probably best to be courteous at the end.
		WP_CLI::success( 'SVG Uploads enabled.' );

	}
}

WP_CLI::add_command( 'produce svgupload', 'SVG_Upload_Command' );
