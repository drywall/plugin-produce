<?php
/**
 * Functions related to the 'sections' Produce command.
 */

/**
 * Tells SearchWP to not index custom fields that begin with an underscore.
 */
function crate_searchwp_omit_meta_key( $omit, $meta_key, $the_post ) {

	// don't index anything marked as private (starts with _)
	if ( '_' === substr( $meta_key, 0, 1 ) ) {
		$omit = true;
	}

	return $omit;
}
add_filter( 'searchwp_omit_meta_key', 'crate_searchwp_omit_meta_key', 10, 3 );


/**
 * Defines initial SearchWP Engine settings that include indexing all custom fields by default.
 */
function crate_searchwp_initial_engine_settings( $settings ) {

	// $settings contains all of the default engine settings
	// you can customize the weights in any way you see fit
	// but do not alter the structure of the array, just values
	$settings['default']['page']['weights']['cf'] = array(
		uniqid( 'swp', true ) => array(
			'metakey' => 'searchwp cf default',
			'weight' => 1,
		),
	);
	$settings['default']['post']['weights']['cf'] = array(
		uniqid( 'swp', true ) => array(
			'metakey' => 'searchwp cf default',
			'weight' => 1,
		),
	);

	return $settings;
}
add_filter( 'searchwp_initial_engine_settings', 'crate_searchwp_initial_engine_settings' );


/**
 * Dynamically generates a list of available post types to be displayed as checkboxen
 * in the 'Content Collection (dynamic)' section.
 */
function crate_acf_post_types_list( $field ) {

	$field['choices'] = array();

	$available_types = get_post_types( array( 'public' => true ), 'objects' );
	foreach ( $available_types as $post_type ) {
		$field['choices'][ $post_type->name ] = $post_type->label;
	}

	return $field;
}
add_filter( 'acf/load_field/name=content_post_types', 'crate_acf_post_types_list' );


/**
 * Enqueues a JS file that pre-defines some colors for the color picker within Sections
 */
function crate_sections_colors() {
	wp_enqueue_script( 'acf-custom-section-colors', get_template_directory_uri() . '/js/sections-admin.js', 'acf-input', '1.0', true );
}
add_action( 'acf/input/admin_enqueue_scripts', 'crate_sections_colors' );


/**
 * Template tag to return an ID attribute for a content section, if a 'section title' was provided
 */
function crate_get_section_id_attr() {

	if ( $section_name = get_sub_field( 'section_title' ) ) {
		return ' id="' . esc_attr( sanitize_title( $section_name ) ) . '"';
	}

	return '';
}
