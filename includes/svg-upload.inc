<?php
/**
 * Sometimes clients need to be able to add SVGs to their media library.
 * It's actually not the most secure thing in the world, but we need to support it.
 */

/**
 * Allow upload of SVG images.
 */
function crate_upload_mimes( $existing_mimes = array() ) {
	$existing_mimes['svg'] = 'image/svg+xml';
	return $existing_mimes;
}
add_filter( 'upload_mimes', 'crate_upload_mimes' );
