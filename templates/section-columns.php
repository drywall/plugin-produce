<?php
/**
 * The template for displaying Generic WYSIWYG sections.
 */

$columns = (int) get_sub_field( 'columns' ); ?>

<div class="columns-wrapper columns-<?php echo esc_attr( $columns ); ?>">

	<?php for ( $col = 1; $col <= $columns; $col++ ) : ?>

		<div class="column">

			<?php the_sub_field( 'column_content_' . $col ); ?>

		</div>

	<?php endfor; ?>

</div>
