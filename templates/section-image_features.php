<?php
/**
 * The template for displaying "Image Feature" sections.
 */

$items = (int) get_sub_field( 'items' );

// Setup the size of images we'll be showing.
// System first looks for a defined custom image size with the name 'image_feature', but that's filterable
$image_size_name       = apply_filters( 'crate_image_feature_size_name', 'image_feature' );

// If the size name isn't found, it looks for 400px square, also filterable.
// This works MUCH better with WP Thumb plugin in place to generate images on-demand.
$image_size_dimensions = apply_filters( 'crate_image_feature_size_dimensions', 'width=400&height=400&crop=1' );

// Finally, figure out whether to use the name or the dimentions
$image_size = ( in_array( $image_size_name, get_intermediate_image_sizes(), true ) ) ? $image_size_name : $image_size_dimensions;

?>

<div class="features-wrapper features-<?php echo esc_attr( $items ); ?>">

	<?php while ( have_rows( 'items' ) ) : the_row(); ?>

		<div class="column feature">

			<div class="feature-wrap">

				<h3><?php the_sub_field( 'feature_heading' ); ?></h3>

				<?php

					// Get the ID of the image to show
					$image_id = get_sub_field( 'feature_image' );

					// Get the image markup
					$image  = wp_get_attachment_image_src( $image_id, $image_size );

					if ( $image ) : ?>

						<a title="<?php echo esc_attr( get_sub_field( 'feature_heading' ) ); ?>" href="<?php the_sub_field( 'feature_button_url' ); ?>"><img class="feature-image" src="<?php echo esc_url( $image[0] ); ?>" alt="<?php echo esc_attr( get_sub_field( 'feature_heading' ) ); ?>" width="<?php echo esc_attr( $image[1] ); ?>" height="<?php echo esc_attr( $image[2] ); ?>" /></a>

					<?php endif;

				?>

				<?php if ( get_sub_field( 'feature_button_text' ) && get_sub_field( 'feature_button_url' ) ) : ?>

					<div class="button-wrapper">
						<a href="<?php the_sub_field( 'feature_button_url' ); ?>" class="button"><?php the_sub_field( 'feature_button_text' ); ?></a>
					</div>

				<?php endif; ?>

			</div>

		</div>

	<?php endwhile; ?>

</div>
