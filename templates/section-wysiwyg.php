<?php
/**
 * The template for displaying Generic WYSIWYG sections.
 */

the_sub_field( 'wysiwyg_content' );
