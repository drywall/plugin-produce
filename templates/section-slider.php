<?php
/**
 * The template for displaying "Image Feature" sections.
 */

$items = (int) get_sub_field( 'items' );

// Setup the size of images we'll be showing.
// System first looks for a defined custom image size with the name 'slide', but that's filterable
$image_size_name       = apply_filters( 'crate_slider_size_name', 'slide' );

// If the size name isn't found, it looks for 980x500, also filterable.
// This works MUCH better with WP Thumb plugin in place to generate images on-demand.
$image_size_dimensions = apply_filters( 'crate_slider_size_dimensions', 'width=980&height=500&crop=1' );

// Finally, figure out whether to use the name or the dimentions
$image_size = ( in_array( $image_size_name, get_intermediate_image_sizes(), true ) ) ? $image_size_name : $image_size_dimensions;

?>

<div class="slider-wrapper">

	<ul class="slider cycle-slideshow"
		data-cycle-timeout="<?php echo esc_attr( get_sub_field( 'slider_timeout' ) * 1000 ); ?>"
		data-cycle-delay="<?php echo esc_attr( get_sub_field( 'slider_delay' ) * 1000 ); ?>"
		data-cycle-fx="<?php echo esc_attr( get_sub_field( 'slider_fx' ) ); ?>"
		data-cycle-pause-on-hover="<?php echo ( get_sub_field( 'slider_pause_on_hover' ) ) ? 'true' : 'false' ; ?>"
		data-cycle-pager="+ .cycle-pager"
		data-cycle-slides="> li"
		data-cycle-swipe="true"
		data-cycle-swipe-fx="scrollHorz"
		data-cycle-prev="~ .cycle-controls > .cycle-prev"
		data-cycle-next="~ .cycle-controls > .cycle-next"
	>

	<?php while ( have_rows( 'slides' ) ) : the_row(); ?>

		<?php $image_id = get_sub_field( 'image' ); ?>

		<li>

			<?php if ( $image_id ) : ?>

				<div class="slide-image-wrap">
					<?php echo wp_get_attachment_image( $image_id, $image_size, false, array( 'class' => 'img-responsive slide-image' ) ); ?>
				</div>

			<?php endif; ?>

			<div class="slide-inner">

				<h3 class="slide-heading"><?php the_sub_field( 'heading' ); ?></h3>

				<div class="slide-caption"><?php the_sub_field( 'content' ); ?></div>

				<?php

					// Get the ID of the image to show
					$image_id = get_sub_field( 'feature_image' );

					// Get the image markup
					$image  = wp_get_attachment_image_src( $image_id, $image_size );

					if ( $image ) : ?>

						<a title="<?php echo esc_attr( get_sub_field( 'feature_heading' ) ); ?>" href="<?php the_sub_field( 'feature_button_url' ); ?>"><img class="feature-image" src="<?php echo esc_url( $image[0] ); ?>" alt="<?php echo esc_attr( get_sub_field( 'feature_heading' ) ); ?>" width="<?php echo esc_attr( $image[1] ); ?>" height="<?php echo esc_attr( $image[2] ); ?>" /></a>

					<?php endif;

				?>

				<?php if ( get_sub_field( 'button_text' ) && get_sub_field( 'button_url' ) ) : ?>

					<div class="button-wrapper">
						<a href="<?php the_sub_field( 'button_url' ); ?>" class="button"><?php the_sub_field( 'button_text' ); ?></a>
					</div>

				<?php endif; ?>

			</div>

		</li>

	<?php endwhile; ?>
	</ul>

	<div class="cycle-pager"></div>

	<div class="cycle-controls">
		<button class="cycle-prev">
			<svg class="arrow arrow-left" viewBox="0 0 30 60" preserveAspectRatio="xMidYMid meet"><path class="a1" d="M30,0-0,30,30,60"></path></svg>
			<span class="assistive-text">Previous</span>
		</button>
		<button class="cycle-next">
			<svg class="arrow arrow-right" viewBox="0 0 30 60" preserveAspectRatio="xMidYMid meet"><path class="a1" d="M-0,60,30,30-0,0"></path></svg>
			<span class="assistive-text">Next</span></button>
	</div>

</div>
