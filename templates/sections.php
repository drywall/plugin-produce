<?php
/**
 * The routing template for displaying section-based content.
 */

if ( have_rows( 'sections' ) ) :

	while ( have_rows( 'sections' ) ) :

		// Get the row
		the_row();

		// Get the layout from the section_type flexible content. There can be only one.
		while ( have_rows( 'section_type' ) ) :
			the_row();
			$section_type = get_row_layout();
		endwhile;

		// Set up variables for the wrapper
		$section_classes = array();
		$section_classes[] = 'type-'   . sanitize_html_class( $section_type );
		$section_classes[] = 'scheme-' . sanitize_html_class( get_sub_field( 'section_text_scheme' ) );
		$section_classes[] = 'width-'  . sanitize_html_class( get_sub_field( 'section_width' ) );
		$section_bg_color  = sanitize_hex_color( get_sub_field( 'section_bg_color' ) );
		?>

		<section class="content-section <?php echo implode( ' ', $section_classes ); ?>" <?php echo crate_get_section_id_attr(); ?> style="background-color: <?php echo $section_bg_color; ?>">

			<div class="section-wrapper">

				<?php if ( get_sub_field( 'section_title_display' ) ) : ?>
					<h2 class="section-title"><?php the_sub_field( 'section_title' ); ?></h2>
				<?php endif; ?>

				<?php while ( have_rows( 'section_type' ) ) : the_row(); ?>
					<?php get_template_part( 'template-parts/section', $section_type ); ?>
				<?php endwhile; ?>

			</div>

		</section>
		<?php

	endwhile;

endif;
