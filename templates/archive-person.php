<?php
/**
 * The archive template for Person CPT
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Produce
 */

get_header(); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			if ( have_posts() ) :

				echo '<div class="style-grid person-loop">';

				/* Start the Loop */
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', 'person' );

				endwhile;

				echo '</div>'; // End .style-grid

				the_posts_navigation();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
