<?php
/**
 * Template part for displaying person posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Produce
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">

		<div class="person-thumbnail-container">
			<?php

			if ( ! is_single() ) {
				echo '<a href="' . esc_url( get_permalink() ) . '">';
			}//end if

			the_post_thumbnail( 'thumbnail' );

			if ( ! is_single() ) {
				echo '</a>';
			}//end if

			?>
		</div>

		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		echo '<h4 class="person-position">' . the_field( 'person_position' ) . '</h4>';
		?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		if ( is_single() ) :

			if ( get_field( 'person_associated_user' ) && count_user_posts( get_field( 'person_associated_user' ) ) ) :
				echo '<a href="' . crate_person_display_author_posts_link() . '">' . __( 'View all posts by', 'crate' ) . ' ' .  crate_person_display_name() . '</a>';
			endif;

			echo crate_person_display_contact_information();

			echo crate_person_display_social_media_profiles();

			echo crate_person_display_additional_fields();

			the_content();
		else :
			echo '<a href="' . esc_url( get_permalink() ) . '" class="button">' . __( 'More about', 'crate' ) . ' ' . crate_person_display_name() . '</a>';
		endif;
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
